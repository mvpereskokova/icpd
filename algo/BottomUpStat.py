import scipy.stats as ss

from ruptures.base import BaseEstimator
from ruptures.utils import pairwise
from .Union import Union


class BottomUpStat(BaseEstimator):
    def __init__(
        self,
        test="mannwhitneyu",
        min_size=100,
        jump=10,
        epsilon=None,
    ):
        assert epsilon != None

        self.test = None
        if test == "mannwhitneyu":
            self.test = ss.mannwhitneyu
        else:
            self.test = ss.kstest

        self.jump = jump
        self.min_size = min_size
        self.n_samples = None
        self.signal = None
        self.leaves = None
        self.epsilon = epsilon
        self.algoName = "BottomUp_2"

    def name(self):
        return self.algoName

    def setName(self, name):
        self.algoName = name

    def _grow_tree(self):
        leaves = list()
        for start in range(0, self.n_samples, self.jump):
            leaves.append((start, min(self.n_samples, start + self.jump)))
        return leaves

    def merge(self, left, right):
        assert left[1] == right[0], "Segments are not contiguous."
        start, mid, end = left[0], left[1], right[1]
        _, pvalue = self.test(self.signal[start:mid], self.signal[mid + 1 : end])
        return ((start, end), pvalue)

    def _seg(self):
        leaves = sorted(self.leaves)

        while True:
            if len(leaves) < 2:
                break

            pvalue, leaf = 0, None

            for left, right in pairwise(leaves):
                candidate, cur_pvalue = self.merge(left, right)
                if cur_pvalue > pvalue:
                    pvalue, leaf = cur_pvalue, candidate

            if pvalue <= self.epsilon:
                break

            for i in range(len(leaves)):
                if leaf[0] == leaves[i][0]:
                    del leaves[i]
                    break

            for i in range(len(leaves)):
                if leaf[1] == leaves[i][1]:
                    del leaves[i]
                    break

            leaves.append(leaf)
            leaves = sorted(leaves)

        partition = {(leaf[0], leaf[1]) for leaf in leaves}
        return partition

    def fit(self, signal) -> "BottomUpStat":
        self.signal = signal
        if signal.ndim == 1:
            (n_samples,) = signal.shape
        else:
            n_samples, _ = signal.shape
        self.n_samples = n_samples
        self.leaves = self._grow_tree()
        return self

    def predict(self):
        partition = self._seg()
        bkps = sorted(e for _, e in partition)
        return Union(bkps[:-1], self.min_size)

    def fit_predict(self, signal):
        self.fit(signal)
        return self.predict()
