def Union(bkps, delta=40, min_size=20):
    result = []
    if not bkps:
        return result

    first, last = bkps[0], bkps[0]

    for elem in bkps[1:]:
        len = last - first
        if elem - last < delta:
            last = elem
        else:
            delta = 0
            if len < min_size:
                delta = (min_size - len) / 2

            result.append(first - delta)
            result.append(last + delta)
            first, last = elem, elem
    len = last - first
    delta = 0
    if len < min_size:
        delta = (min_size - len) / 2

    result.append(first - delta)
    result.append(last + delta)
    return result
