import scipy.stats as ss

from ruptures.base import BaseCost, BaseEstimator
from ruptures.costs import cost_factory
from ruptures.utils import pairwise
from ruptures.exceptions import BadSegmentationParameters
from .Union import Union


class BottomUpCost(BaseEstimator):
    def __init__(
        self,
        model="l2",
        custom_cost=None,
        min_size=100,
        jump=40,
        params=None,
        epsilon=None,
    ):
        if custom_cost is not None and isinstance(custom_cost, BaseCost):
            self.cost = custom_cost
        else:
            if params is None:
                self.cost = cost_factory(model=model)
            else:
                self.cost = cost_factory(model=model, **params)

        assert epsilon != None
        self.jump = jump
        self.min_size = max(min_size, self.cost.min_size)
        self.n_samples = None
        self.signal = None
        self.leaves = None
        self.epsilon = epsilon
        self.algoName = "BottomUp1"

    def name(self):
        return self.algoName

    def setName(self, name):
        self.algoName = name

    def _grow_tree(self):
        leaves = list()
        for start in range(0, self.n_samples, self.jump):
            leaves.append((start, min(self.n_samples, start + self.jump)))
        return leaves

    def merge(self, left, right):
        assert left[1] == right[0], "Segments are not contiguous."
        start, mid, end = left[0], left[1], right[1]
        value = (
            self.cost.error(start, end)
            - self.cost.error(start, mid)
            - self.cost.error(mid, end)
        )
        return ((start, end), value)

    def _seg(self):
        leaves = sorted(self.leaves)

        while len(leaves) >= 2:
            value, leaf = 10000000000, None
            for left, right in pairwise(leaves):
                candidate, cur_value = self.merge(left, right)
                if cur_value < value:
                    value, leaf = cur_value, candidate
            if value >= self.epsilon:
                break

            for i in range(len(leaves)):
                if leaf[0] == leaves[i][0]:
                    del leaves[i]
                    break

            for i in range(len(leaves)):
                if leaf[1] == leaves[i][1]:
                    del leaves[i]
                    break

            leaves.append(leaf)
            leaves = sorted(leaves)

        partition = {(leaf[0], leaf[1]) for leaf in leaves}
        return partition

    def fit(self, signal) -> "BottomUpCost":
        self.cost.fit(signal)
        self.signal = signal
        if signal.ndim == 1:
            (n_samples,) = signal.shape
        else:
            n_samples, _ = signal.shape
        self.n_samples = n_samples
        self.leaves = self._grow_tree()
        return self

    def predict(self):
        if self.min_size > self.cost.signal.shape[0]:
            raise BadSegmentationParameters

        partition = self._seg()
        bkps = sorted(e for _, e in partition)
        return Union(bkps[:-1], self.min_size)

    def fit_predict(self, signal):
        self.fit(signal)
        return self.predict()
