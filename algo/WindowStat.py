import numpy as np
import scipy.stats as ss

from ruptures.base import BaseCost, BaseEstimator
from ruptures.costs import cost_factory

from .Union import Union


class WindowStat(BaseEstimator):
    def __init__(self, test="mannwhitneyu", width=100, epsilon=0.05):
        self.test = None
        if test == "mannwhitneyu":
            self.test = ss.mannwhitneyu
        else:
            self.test = ss.kstest

        self.width = 2 * (width // 2)
        self.n_samples = None
        self.signal = None
        self.inds = None
        self.pvalue = list()
        self.epsilon = epsilon
        self.algoName = "WindowStat"

    def name(self):
        return self.algoName

    def setName(self, name):
        self.algoName = name

    def _seg(self):
        bkps = [self.n_samples]
        peak_inds = np.argsort(self.pvalue)
        while len(peak_inds) > 0:
            peak = peak_inds[0]
            peak_inds = peak_inds[1:]
            if self.pvalue[peak] > self.epsilon:
                break

            bkps.append(self.inds[peak])

        bkps.sort()
        return Union(bkps[:-1])

    def fit(self, signal) -> "WindowStat":
        self.signal = signal

        self.n_samples = self.signal.shape[0]
        self.inds = np.arange(self.n_samples, step=1)
        keep = (self.inds >= self.width // 2) & (
            self.inds < self.n_samples - self.width // 2
        )
        self.inds = self.inds[keep]

        # compute score
        score = list()
        for k in self.inds:
            start, mid, end = k - self.width // 2, k, k + self.width // 2
            _, pvalue = self.test(self.signal[start:mid], self.signal[mid + 1 : end])
            score.append(pvalue)

        self.pvalue = np.array(score)
        return self

    def predict(self):
        bkps = self._seg()
        return bkps

    def fit_predict(self, signal):
        self.fit(signal)
        return self.predict()
